from subprocess import CompletedProcess
import subprocess
import re
import base64

def main() -> str:
    '''Returns the flag.'''
    # Run strings on the binary.
    process: CompletedProcess = subprocess.run(['strings', './cat.jpg'], check=True, capture_output=True)
    # Match the license resource field and extract the base64 encoded value.
    encoded_flag: str = re.search(r'resource=\'\s*(.*)\'', \
                                   process.stdout.decode()).group(1)
    # Decode the base64 encoded value.
    flag: str = base64.decodebytes(encoded_flag.encode()).decode()
    return flag

if __name__ == '__main__':
    flag: str = main()
    print(f'Flag: {flag}')